﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class FollowCamera : MonoBehaviour 
{
	[SerializeField] private Transform Player;
	[SerializeField] private float smooth = 0.3F;

	private Vector3 offset;
	private Vector3 velocity = Vector3.zero;
    private Vector3 PlayerPosition;


    void Start()
	{
	  offset = new Vector3 ((transform.position.x - Player.transform.position.x)*2,
	  transform.position.y - Player.position.y, transform.position.z - Player.position.z);
	}
    

	void FixedUpdate()
    {
        PlayerPosition.x = Player.position.x;
        PlayerPosition.y = Player.position.y;
      
        Vector3 targetPosition = new Vector3(Player.position.x-5f + offset.x, Player.position.y +1f + offset.y, 0 + offset.z);
		transform.position = Vector3.SmoothDamp(transform.position, targetPosition, ref velocity, smooth);
	}
}
