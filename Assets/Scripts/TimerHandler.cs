﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
public class TimerHandler : MonoBehaviour {

    [SerializeField] private Text _timer;
    [SerializeField] private SpawnCubesController _spawnCubesController;

    private float timeLeft;
    private bool playTimer = false;

    void FixedUpdate () {
        if(playTimer==true)
        {
            timeLeft -= Time.deltaTime * 0.7142857142857143f; //deltatime * 1f/1.4f(timeScale)

            string minutes = Mathf.Floor(timeLeft / 60).ToString("00");
            string seconds = (timeLeft % 60).ToString("00");

            if (timeLeft>=0)
            _timer.text = string.Format("{0}:{1}", minutes, seconds);

            if (timeLeft < 0)
            {
                playTimer = false;
                _spawnCubesController.SpawnLastFloor();
            }

        }
    }

    public void StartTimer()
    {
        timeLeft = SingletonManager.instance.songTime;
        playTimer = true;
        _timer.DOFade(0.5f, 1);
    }
    public void StopTimer()
    {
        playTimer = false;
        timeLeft = 0f;
        _timer.DOFade(0, 1);
    }
}
