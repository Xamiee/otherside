﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class DoTweenController : MonoBehaviour 
{
    [SerializeField] private GameObject _camera;
    [SerializeField] private GameObject _mainmenuContainer;
    [SerializeField] private Image _splashScreenImage;
    [SerializeField] private Image _splashScreenTitle;
    [SerializeField] private Text _splashScreenTapToStart;

    public void Start()
    {
        TapTextScalling();
    }

    public void HideSplashScreen()
    {
        if (_splashScreenImage.color.a==1)
        {
            _splashScreenImage.raycastTarget = false;
            _splashScreenTitle.raycastTarget = false;

            _splashScreenImage.DOFade(0f, 0.5f).SetDelay(0f).SetSpeedBased(true);
            _splashScreenTitle.DOFade(0f, 0.5f).SetDelay(0f).SetSpeedBased(true);
            _splashScreenTapToStart.DOFade(0f, 0.5f).SetDelay(0f).SetSpeedBased(true);

           StartCoroutine(EnableCamera(0.1f));
        }
    }

    IEnumerator EnableCamera(float time)
    {
        yield return new WaitForSeconds(time);
        _camera.SetActive(true);
    }

    public void HideMenu()
    {
        _mainmenuContainer.SetActive(false);
        
        //Additional function for better performance

        if(_splashScreenImage.color.a ==0)
        {
            _splashScreenImage.transform.parent.gameObject.SetActive(false);
        }
    }
    
    public void ShowMenu()
    {
        _mainmenuContainer.SetActive(true);
    }

    public void TapTextScalling()
    {
        _splashScreenTapToStart.gameObject.transform.DOScale(1.1f,0.15f).SetLoops(-1,LoopType.Yoyo).SetEase(Ease.Linear).SetSpeedBased(true);
    }
}
