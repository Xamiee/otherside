﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FPS_Time_Controller : MonoBehaviour {
    
	void Start ()
    {
        Time.timeScale = Time.timeScale * 1.4f;
        QualitySettings.vSyncCount = 0;
        Application.targetFrameRate = 60;
    }
	

}
