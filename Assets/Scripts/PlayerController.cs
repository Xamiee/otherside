﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour {


    [SerializeField] private Animator anim;
    [SerializeField] private Rigidbody2D rb2D;

    private bool canJump = false;

    void Update()
    {
       if(rb2D.velocity.y>0.2f ||rb2D.velocity.y<-0.05f)
        {
            canJump = false;
        }

        if ((Input.GetButton("Jump") || Input.GetMouseButton(0)) && canJump == true && SingletonManager.instance.gameover == false)
        {
            anim.SetTrigger("Active");
            rb2D.AddForce(new Vector2(0f, 9f), ForceMode2D.Impulse);
        }
    }

    void FixedUpdate ()
    {
        if(SingletonManager.instance.gameover == false)
        {
            transform.localPosition += new Vector3(0.12f, 0f, 0f);
        }
    }

    void OnCollisionEnter2D(Collision2D collision)
    {
        canJump = true;
    }

}
