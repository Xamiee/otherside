﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class PrefabPooler : MonoBehaviour
{

    public static PrefabPooler Instance;
    [HideInInspector] public List<GameObject> pooledBlockx1;
    [HideInInspector] public List<GameObject> pooledBlockx2;
    [HideInInspector] public List<GameObject> pooledBlockx3;
    [SerializeField] private GameObject prefabBlockx1;
    [SerializeField] private GameObject prefabBlockx2;
    [SerializeField] private GameObject prefabBlockx3;
    [SerializeField] private int amountBlockx1_ToPool;
    [SerializeField] private int amountBlockx2_ToPool;
    [SerializeField] private int amountBlockx3_ToPool;

    void Awake()
    {
        Instance = this;
    }

    void Start()
    {
        pooledBlockx1 = new List<GameObject>();
        for (int i = 0; i < amountBlockx1_ToPool; i++)
        {
            GameObject obj = (GameObject)Instantiate(prefabBlockx1);
            //obj.transform.parent = transform.parent;
            obj.SetActive(false);
            pooledBlockx1.Add(obj);
        }

        pooledBlockx2 = new List<GameObject>();
        for (int i = 0; i < amountBlockx2_ToPool; i++)
        {
            GameObject obj = (GameObject)Instantiate(prefabBlockx2);
            //obj.transform.parent = transform.parent;
            obj.SetActive(false);
            pooledBlockx2.Add(obj);
        }

        pooledBlockx3 = new List<GameObject>();
        for (int i = 0; i < amountBlockx3_ToPool; i++)
        {
            GameObject obj = (GameObject)Instantiate(prefabBlockx3);
            //obj.transform.parent = transform.parent;
            obj.SetActive(false);
            pooledBlockx3.Add(obj);
        }
    }


    public GameObject GetPooled_Blockx1()
    {
        for (int i = 0; i < pooledBlockx1.Count; i++)
        {
            if (!pooledBlockx1[i].activeInHierarchy)
            {
                return pooledBlockx1[i];
            }
        }
        return null;
    }

    public GameObject GetPooled_Blockx2()
    {
        for (int i = 0; i < pooledBlockx2.Count; i++)
        {
            if (!pooledBlockx2[i].activeInHierarchy)
            {
                return pooledBlockx2[i];
            }
        }
        return null;
    }

    public GameObject GetPooled_Blockx3()
    {
        
        for (int i = 0; i < pooledBlockx3.Count; i++)
        {
            if (!pooledBlockx3[i].activeInHierarchy)
            {
                return pooledBlockx3[i];
            }
        }
        return null;
    }
}
