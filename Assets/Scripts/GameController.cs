﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameController : MonoBehaviour {

    [SerializeField] private Transform _blocksParent;
    [SerializeField] private Transform _cameraObject;
    [SerializeField] private Transform _player;
    [SerializeField] private Transform _spawnPoint;
    [SerializeField] private AudioSource _playerAudioSource;
    [SerializeField] private DoTweenController _doTween;
    [SerializeField] private SpawnCubesController _spawnCubeController;
    [SerializeField] private TimerHandler _timerHandler;
    
    private Vector3 playerStartPosition;
    private Vector3 cameraStartPosition;
	
	void LateUpdate () 
	{
        if((_cameraObject.transform.position.y - _player.transform.position.y)>15 && SingletonManager.instance.gameover==false)
            {
                EndGame();
            }
        
	}

    public void StartGame()
    {
        SingletonManager.instance.gameover = false;
        SingletonManager.instance.songTime = 144f; //music1(distant lands) == 2:24

        ResetAudio();
        
        _spawnCubeController.StartSpawning();
        _doTween.HideMenu();
        _playerAudioSource.pitch = 1f;
    }

    public void EndGame()
    {
        SingletonManager.instance.gameover = true;
        
        ResetPosition();

        _timerHandler.StopTimer();
        _doTween.ShowMenu();
        _spawnCubeController.StopAllCoroutines();
        _playerAudioSource.pitch = 0.1f;
        
    }
    
    private void ResetAudio()
    {
        _playerAudioSource.Stop();
        _playerAudioSource.PlayDelayed(0);
    }

    private void ResetPosition()
    {
        _player.gameObject.SetActive(false);
        _cameraObject.gameObject.SetActive(false);

        _player.position = _spawnPoint.position;
        _cameraObject.position = _spawnPoint.position + new Vector3(5, 3, 0);

        _player.gameObject.SetActive(true);
        _cameraObject.gameObject.SetActive(true);
    }
    
}
