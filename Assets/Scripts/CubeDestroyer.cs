﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CubeDestroyer : MonoBehaviour {

    void OnEnable()
    {
        StartCoroutine(DestroyCube(4));
    }

    IEnumerator DestroyCube(float time)
    {
        yield return new WaitForSeconds(time);
        gameObject.SetActive(false);
    }

   
}
