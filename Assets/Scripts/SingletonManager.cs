﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SingletonManager : MonoBehaviour {

    public static SingletonManager instance;


    public bool gameover = true;
    public float songTime;

    void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else if (instance!=null)
        {
            Destroy(gameObject);
        }
    }
}
