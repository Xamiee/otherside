﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraDistance : MonoBehaviour
{
    [SerializeField] private Camera _camera;

    private float horizontalResolution = 1920;

    private void OnGUI()
    {
        float currentAspect = (float)Screen.width / (float)Screen.height;
        _camera.orthographicSize = horizontalResolution / currentAspect / 200;
    }
}