﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
public class SpawnCubesController : MonoBehaviour {
 
    [SerializeField] private GameObject parentSpawnedBlocks;
    [SerializeField] private GameObject Blockx1;
    [SerializeField] private GameObject Blockx2;
    [SerializeField] private GameObject Blockx3;
    [SerializeField] private GameObject firstBlock;
    [SerializeField] private GameObject endingFloor;

    private int randNextBlock;
    private float lastBlockPosition_x;
    private float lastBlockPosition_y;


    public void StartSpawning()
    {
        lastBlockPosition_x = firstBlock.transform.position.x;
        lastBlockPosition_y = firstBlock.transform.position.y;
        StartCoroutine(Timer_SpawnNextBlock(4.95f));

        // StartCoroutine(Timer_SpawnNextBlock(5.7f));
    }

    public void SpawnLastFloor()
    {
        StopAllCoroutines();


        lastBlockPosition_x = lastBlockPosition_x + 4.63f;
        GameObject blockEnding = Instantiate(endingFloor, new Vector3(lastBlockPosition_x+22f, lastBlockPosition_y, 0f), transform.rotation);
        blockEnding.transform.parent = parentSpawnedBlocks.transform;
        blockEnding.transform.DOMoveY(lastBlockPosition_y, 1f).SetEase(Ease.InOutQuad);
    }

    private void SpawnNextBlock()
    {
        randNextBlock = Random.Range(0, 100);

        if (randNextBlock > 75) // Spawning block upside from last spawn position;
        {
            SpawnUpstair();
        }
        else if (randNextBlock <= 75 && randNextBlock > 50) // Spawning block straight from last spawn position;
        {
            SpawnStraight();
        }
        else // Spawning block straight from last spawn position;
        {
            SpawnDownstair();
        }
    }

    private void SpawnUpstair()
    {
        lastBlockPosition_x = lastBlockPosition_x + 3.75f;

        GameObject blockUpper = PrefabPooler.Instance.GetPooled_Blockx1();
        
        if (blockUpper != null)
        {
            blockUpper.transform.position = new Vector3(lastBlockPosition_x, lastBlockPosition_y + 1f, 0f);
            blockUpper.transform.rotation = transform.rotation;
            blockUpper.transform.position += new Vector3(0, -5f, 0);
            blockUpper.transform.DOMoveY(lastBlockPosition_y + 1f, 1f).SetEase(Ease.InOutQuad); //Using DoTween to move objects smooth.
            blockUpper.SetActive(true);
            lastBlockPosition_y = lastBlockPosition_y + 1f;

            StartCoroutine(Timer_SpawnNextBlock(0.51025f));
        }
    }

    private void SpawnStraight()
    {
        int randNumberOfBlocks = Random.Range(1, 4);

        if (randNumberOfBlocks == 1)
        {

            lastBlockPosition_x = lastBlockPosition_x + 4.4325f;
            GameObject blockStraight = PrefabPooler.Instance.GetPooled_Blockx1();
            
            if (blockStraight != null)
            {
                blockStraight.transform.position = new Vector3(lastBlockPosition_x, lastBlockPosition_y, 0f);
                blockStraight.transform.rotation = transform.rotation;
                blockStraight.transform.position += new Vector3(0, -5f, 0);
                blockStraight.transform.DOMoveY(lastBlockPosition_y, 1f).SetEase(Ease.InOutQuad);
                blockStraight.SetActive(true);
                
                StartCoroutine(Timer_SpawnNextBlock(0.6030f));
            }
        }

        if (randNumberOfBlocks == 2)
        {
            lastBlockPosition_x = lastBlockPosition_x + 4.63f;

            GameObject blockStraight = PrefabPooler.Instance.GetPooled_Blockx2();
            if (blockStraight != null)
            {
                blockStraight.transform.position = new Vector3(lastBlockPosition_x, lastBlockPosition_y, 0f);
                blockStraight.transform.rotation = transform.rotation;
                blockStraight.transform.position += new Vector3(0, -5f, 0);
                blockStraight.transform.DOMoveY(lastBlockPosition_y, 1f).SetEase(Ease.InOutQuad);
                blockStraight.SetActive(true);

                lastBlockPosition_x = lastBlockPosition_x + 0.5f;

                StartCoroutine(Timer_SpawnNextBlock(0.7f));

            }
        }

        if (randNumberOfBlocks == 3)
        {
            lastBlockPosition_x = lastBlockPosition_x + 4.63f;

            GameObject blockStraight = PrefabPooler.Instance.GetPooled_Blockx3();
            
            if (blockStraight != null)
            {
                blockStraight.transform.position = new Vector3(lastBlockPosition_x, lastBlockPosition_y, 0f);
                blockStraight.transform.rotation = transform.rotation;
                blockStraight.transform.position += new Vector3(0, -5f, 0);
                blockStraight.transform.DOMoveY(lastBlockPosition_y, 1f).SetEase(Ease.InOutQuad);
                blockStraight.SetActive(true);
                lastBlockPosition_x = lastBlockPosition_x + 1f;

                StartCoroutine(Timer_SpawnNextBlock(0.8f));
            }
        }
    }

    private void SpawnDownstair()
    {
        lastBlockPosition_x = lastBlockPosition_x + 4;

        GameObject blockDownStair = PrefabPooler.Instance.GetPooled_Blockx1();
        
        if (blockDownStair != null)
        {
            blockDownStair.transform.position = new Vector3(lastBlockPosition_x, lastBlockPosition_y - 2f, 0f);
            blockDownStair.transform.rotation = transform.rotation;
            blockDownStair.transform.position += new Vector3(0, -5f, 0);
            blockDownStair.transform.DOMoveY(lastBlockPosition_y - 2f, 1f).SetEase(Ease.InOutQuad);
            blockDownStair.SetActive(true);
            lastBlockPosition_y = lastBlockPosition_y - 2f;

            StartCoroutine(Timer_SpawnNextBlock(0.55f));
        }
    }
    
    IEnumerator Timer_SpawnNextBlock(float Time)
    {
        yield return new WaitForSeconds(Time);

        if (SingletonManager.instance.gameover == false)
        {
            SpawnNextBlock();
        }
    }

}

      
